package android.training.myapplication.viewmodel

import android.content.Intent
import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.facebook.share.model.SharePhoto
import com.facebook.share.model.SharePhotoContent


class CreateFbPhotoPostViewModel : ViewModel() {
    companion object {
        const val REQUEST_CAPTURE_IMAGE = 100
    }

    sealed class CreatePhotoPostCommand {
        class ShareContent(val sharePhotoContent: SharePhotoContent) : CreatePhotoPostCommand()
        object GetPhotoFromGallery : CreatePhotoPostCommand() {
            fun getIntent(): Intent {
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                val mimeTypes = arrayOf("image/jpeg", "image/png")
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)

                return intent
            }
        }
    }

    private val _command : MutableLiveData<CreatePhotoPostCommand> = MutableLiveData()
    val command : LiveData<CreatePhotoPostCommand>
    get() = _command

    fun addPhoto() {
        Log.d("TEST", "addPhoto called from $this")
        _command.value =
            CreatePhotoPostCommand.GetPhotoFromGallery
    }

    fun setupShareablePhoto( bitmap: Bitmap) {
        val photo = SharePhoto.Builder()
            .setBitmap(bitmap)
            .build()
        val content = SharePhotoContent.Builder()
            .addPhoto(photo)
            .build()

        _command.value =
            CreatePhotoPostCommand.ShareContent(
                content
            )
    }

}
