package android.training.myapplication.viewmodel


import android.training.myapplication.R
import android.training.myapplication.view.fragment.LoginFragment
import android.util.Log
import androidx.lifecycle.*
import com.facebook.AccessToken
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult


class LoginViewModel : ViewModel(), LifecycleObserver {

    private val _command: MutableLiveData<LoginCommand> = MutableLiveData()
    val command: LiveData<LoginCommand>
        get() = _command

    sealed class LoginCommand {
        class Navigate(val destination: Int) : LoginCommand()
        object LoginWithReadPermissions : LoginCommand()
        object Refresh : LoginCommand()
    }

    fun resetCommandToNull() {
        _command.value = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun checkForValidTokenAndPermissions() {
        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedIn = accessToken != null && !accessToken.isExpired
        if (isLoggedIn) {
            Log.d("TEST", "Valid access token found")
            if (accessToken.permissions.containsAll(LoginFragment.READ_PERMISSIONS_LIST)) {
                _command.value =
                    LoginCommand.Navigate(
                        R.id.action_loginFragment_to_userPhotosFragment
                    )
            } else {
                _command.value =
                    LoginCommand.LoginWithReadPermissions
            }
        }
    }

    class FbLoginCallback constructor(var viewModel: LoginViewModel) :
        FacebookCallback<LoginResult> {

        override fun onSuccess(result: LoginResult?) {
            Log.d("TEST", "onSuccess called from $this")
            //TODO : add a dialog for the missing permissions and then relog
            viewModel.checkForValidTokenAndPermissions()
        }

        override fun onCancel() {
            Log.d("TEST", "onCancel called from $this")
        }

        override fun onError(error: FacebookException?) {
            Log.d("TEST", error?.localizedMessage)
        }
    }

}
