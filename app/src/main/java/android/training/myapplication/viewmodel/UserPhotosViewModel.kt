package android.training.myapplication.viewmodel

import android.content.Context
import android.training.myapplication.repository.PhotoRepository
import android.training.myapplication.R
import android.training.myapplication.model.UserModel
import android.training.myapplication.repository.UserRepository
import android.training.myapplication.utils.background.RefreshPhotosWorker
import android.training.myapplication.view.fragment.UserPhotosFragment
import android.util.Log
import androidx.lifecycle.*
import androidx.work.*
import java.util.concurrent.TimeUnit

class UserPhotosViewModel(appContext: Context) : ViewModel(), LifecycleObserver {
    sealed class UserPhotoCommand {
        class Navigate(val destination: Int) : UserPhotoCommand()
        object ObserveUserDao : UserPhotoCommand()
    }

    private val context =
        appContext // just to see how you can pass stuff into view model constructors

    private var photoRepository = PhotoRepository()
    private var userRepository = UserRepository()
    var shouldRefreshPhotoList: Boolean = false

    private lateinit var photoWorkerRequest: PeriodicWorkRequest

    private val _command: MutableLiveData<UserPhotoCommand> = MutableLiveData()
    val command: LiveData<UserPhotoCommand>
        get() = _command


    private val _photoIdList: MutableLiveData<ArrayList<String>> = photoRepository.photoSourcesList
    val photoIdList: LiveData<ArrayList<String>>
        get() = _photoIdList

    fun addPostFromFab() {
        _command.value =
            UserPhotoCommand.Navigate(
                R.id.action_userPhotosFragment_to_createFbPhotoPostFragment
            )
        shouldRefreshPhotoList = true //todo: set the value to true after user clicks the share button
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun startAfterFacebookLoginFlow() {
        userLoginFlowStart()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        Log.d("TEST", "onPause called")
        photoRepository.cancelOngoingRequests()
        userRepository.cancelOngoingRequests()
    }

    fun userLoginFlowStart() {
        userRepository.saveUser()
        onUserSavedInDB()
    }

    fun onUserSavedInDB() {
        _command.value = UserPhotoCommand.ObserveUserDao
    }

    fun loginFlowOnUserLoggedIn(userModel: UserModel) {
        photoRepository.saveOrGetSavedPhotoSources(userModel)
        if (!::photoWorkerRequest.isInitialized) {
            registerPhotoWorker(userModel)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun refreshPhotoListAfterPosting() {
        if (shouldRefreshPhotoList){
            photoRepository.getAndRefreshPhotoList()
        }
    }

    private fun registerPhotoWorker(userModel: UserModel) {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setRequiresBatteryNotLow(true)
            .build()

        val data = Data.Builder()
        data.putString(RefreshPhotosWorker.REFRESH_WORKER_USER_ID_KEY, userModel.id)

        val refreshPhotosWorkerBuilder =
            PeriodicWorkRequest.Builder(RefreshPhotosWorker::class.java, 15, TimeUnit.MINUTES)
        refreshPhotosWorkerBuilder.setConstraints(constraints)

        photoWorkerRequest = refreshPhotosWorkerBuilder
            .setInputData(data.build())
            .setBackoffCriteria(BackoffPolicy.LINEAR, 1, TimeUnit.MINUTES)
            .build()
        WorkManager.getInstance().enqueueUniquePeriodicWork(
            UserPhotosFragment.TAG_REFRESH_PHOTOS_WORKER,
            ExistingPeriodicWorkPolicy.KEEP,
            photoWorkerRequest
        )
    }

}
