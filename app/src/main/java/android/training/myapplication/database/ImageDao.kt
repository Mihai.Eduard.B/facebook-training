package android.training.myapplication.database

import android.training.myapplication.model.ImageModel
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ImageDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(image: ImageModel.Image?)

    @Query("SELECT * FROM image WHERE ownerId IS (:id) LIMIT 1")
    suspend fun getImageByPhotoId(id: String) : ImageModel.Image?

    @Query("SELECT * FROM image")
    fun getAllImages() : LiveData<List<ImageModel.Image>>
}