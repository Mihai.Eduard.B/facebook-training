package android.training.myapplication.database

import android.content.Context
import android.training.myapplication.model.ImageModel
import android.training.myapplication.model.UserModel
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(ImageModel.Image::class, UserModel::class), version = 1)
abstract class FbRoomDatabase : RoomDatabase(){
    abstract fun imageDao() : ImageDao
    abstract fun userDao() : UserDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: FbRoomDatabase? = null

        fun getDatabase(context: Context): FbRoomDatabase {
            val tempInstance =
                INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    FbRoomDatabase::class.java,
                    "database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}