package android.training.myapplication.database

import android.training.myapplication.model.UserModel
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(user: UserModel)

    @Query("SELECT * FROM usermodel WHERE id IS (:id) LIMIT 1")
    suspend fun getUserById(id: String?): UserModel

    @Query("SELECT * FROM usermodel LIMIT 1")
    fun getUser(): LiveData<UserModel>

}