package android.training.myapplication.view.fragment

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.training.myapplication.view.adapter.PhotoAdapter
import android.training.myapplication.R
import android.training.myapplication.database.FbRoomDatabase
import android.training.myapplication.viewmodel.UserPhotosViewModel
import android.training.myapplication.databinding.UserPhotosFragmentBinding
import android.training.myapplication.model.UserModel
import android.training.myapplication.utils.application.FBApp
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.facebook.login.LoginManager
import kotlinx.android.synthetic.main.login_fragment.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


class UserPhotosFragment : Fragment(), CoroutineScope {
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    companion object {
        fun newInstance() = UserPhotosFragment()
        const val TAG_REFRESH_PHOTOS_WORKER: String = "TAG_REFRESH_PHOTOS_WORKER"
    }

    private lateinit var binding: UserPhotosFragmentBinding
    private lateinit var viewModel: UserPhotosViewModel
    private lateinit var photoAdaptor: PhotoAdapter

    private inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>): T = f() as T
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory {
            UserPhotosViewModel(
                this.context!!.applicationContext
            )
        }).get(UserPhotosViewModel::class.java)
        photoAdaptor = PhotoAdapter()
        lifecycle.addObserver(viewModel)

        viewModel.command.observe(this, Observer { command ->
            when (command) {
                is UserPhotosViewModel.UserPhotoCommand.Navigate -> {
                    view?.findNavController()?.navigate(command.destination)
                }
                is UserPhotosViewModel.UserPhotoCommand.ObserveUserDao -> {
                    launch {
                        FBApp.roomDatabase.userDao().getUser()
                            .observe(this@UserPhotosFragment, Observer<UserModel> {//TODO: observe when room has as User
                                if (it != null) {
                                    viewModel.loginFlowOnUserLoggedIn(it)
                                }
                            })
                    }
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.user_photos_fragment, container, false
        )
        binding.viewmodel = viewModel
        binding.apply {
            setLifecycleOwner { this@UserPhotosFragment.lifecycle }
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val photoObserver = Observer<ArrayList<String>> {
            //pass it to the recycler view
            Log.d("TEST", "PhotoIdList from photoObserver is: $it")
            updatePhotoIdList(it)
        }
        viewModel.photoIdList.observe(this, photoObserver)

        requireActivity().onBackPressedDispatcher.addCallback(object :
            OnBackPressedCallback(true) { //logout on back button press
            override fun handleOnBackPressed() {
                LoginManager.getInstance().logOut()
                launch {
                    withContext(Dispatchers.IO) {
                        FBApp.roomDatabase.clearAllTables()
                    }
                }
                Log.d("TEST", " User was LoggedOut of FB")
                view?.findNavController()?.navigateUp()
            }
        })
    }

    override fun onPause() {
        super.onPause()
        if (job.isActive) {
            job.cancel()
        }
    }

    private fun initRecyclerView() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@UserPhotosFragment.context)
            adapter = photoAdaptor
        }
    }

    private fun updatePhotoIdList(photoList: ArrayList<String>) {
        photoAdaptor.submitPhotoModelList(photoList)

        initRecyclerView()
    }

}
