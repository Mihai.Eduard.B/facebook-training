package android.training.myapplication.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.training.myapplication.R


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

}
