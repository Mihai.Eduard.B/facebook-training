package android.training.myapplication.view.adapter

import android.training.myapplication.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.photo_item.view.*

class PhotoAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var photoIdList: ArrayList<String> = ArrayList()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PhotoHolder -> {
                holder.bind(photoIdList[position])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoHolder {
        return PhotoHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.photo_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return photoIdList.size
    }

    fun submitPhotoModelList(photoList: ArrayList<String>) {
        photoIdList = photoList
        notifyDataSetChanged()
    }

    class PhotoHolder constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val photoItem: ImageView = itemView.photo_item

        fun bind(photoSource: String) {
            loadImageWithGlide(photoSource)
        }

        private fun loadImageWithGlide(source: String?) {
            Glide.with(itemView.context)
                .setDefaultRequestOptions(RequestOptions()
                    .placeholder(R.drawable.ic_placeholder)
                    .error(R.drawable.ic_error_black_24dp)
                )
                .load(source)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(photoItem)
        }
    }
}