package android.training.myapplication.view.fragment

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.provider.MediaStore
import android.training.myapplication.viewmodel.CreateFbPhotoPostViewModel
import android.training.myapplication.R
import android.training.myapplication.databinding.CreateFbPhotoPostFragmentBinding
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.facebook.*
import kotlinx.android.synthetic.main.create_fb_photo_post_fragment.*


class CreateFbPhotoPostFragment : Fragment() {

    companion object {
        fun newInstance() = CreateFbPhotoPostFragment()
        const val GALLERY_REQUEST_CODE = 100
    }

    private lateinit var viewModel: CreateFbPhotoPostViewModel
    private lateinit var binding: CreateFbPhotoPostFragmentBinding
    private lateinit var postPhotoAsyncTask : GraphRequestAsyncTask
    private var callbackManager: CallbackManager = CallbackManager.Factory.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(CreateFbPhotoPostViewModel::class.java)

        requireActivity().onBackPressedDispatcher.addCallback(object :
            OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                view?.findNavController()?.navigateUp()
                Log.d("TEST", "$this onBackPressed")
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.create_fb_photo_post_fragment,
            container,
            false
        )
        binding.viewmodel = viewModel
        binding.apply {
            setLifecycleOwner { this@CreateFbPhotoPostFragment.lifecycle }
        }
        //setup command observer that may use the view
        binding.root.post {
            viewModel.command.observe(this, Observer {
                when (it) {
                    is CreateFbPhotoPostViewModel.CreatePhotoPostCommand.GetPhotoFromGallery -> {
                        val intent =
                            CreateFbPhotoPostViewModel.CreatePhotoPostCommand.GetPhotoFromGallery.getIntent()
                        startActivityForResult(intent,
                            GALLERY_REQUEST_CODE
                        )
                    }

                    is CreateFbPhotoPostViewModel.CreatePhotoPostCommand.ShareContent ->{
                        share_photo_to_fb.shareContent = it.sharePhotoContent
                        share_photo_to_fb.visibility = View.VISIBLE
                    }
                }
            })
        }
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        if (::postPhotoAsyncTask.isInitialized){
            postPhotoAsyncTask.cancel(true)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                100 -> {
                    val uri: Uri? = data?.data
                    Log.d("TEST", "Photo Uri from onActivityResult = $uri")
                    val imageBitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, uri)
                    viewModel.setupShareablePhoto(imageBitmap)
                }
            }
        }
    }
}
