package android.training.myapplication.view.fragment

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.training.myapplication.databinding.LoginFragmentBinding
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import android.content.Intent
import android.training.myapplication.viewmodel.LoginViewModel
import android.training.myapplication.R
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.facebook.*
import com.facebook.login.LoginManager


class LoginFragment : Fragment() {

    companion object {
        fun newInstance() = LoginFragment()
        val READ_PERMISSIONS_LIST = listOf("user_photos", "user_posts")
    }

    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: LoginFragmentBinding

    private var callbackManager: CallbackManager = CallbackManager.Factory.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        lifecycle.addObserver(viewModel)

        viewModel.command.observe(this, Observer {
            when (it) {
                is LoginViewModel.LoginCommand.Navigate -> {
                    view?.findNavController()?.navigate(it.destination)
                }
                is LoginViewModel.LoginCommand.Refresh -> {
                    //refreshUI
                }
                is LoginViewModel.LoginCommand.LoginWithReadPermissions -> {
                    LoginManager.getInstance().logInWithReadPermissions(activity,
                        READ_PERMISSIONS_LIST
                    )
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,
            R.layout.login_fragment, container, false)
        binding.apply {
            setLifecycleOwner { lifecycle }
            viewmodel = viewmodel
            loginButton.fragment = this@LoginFragment
            loginButton.registerCallback(callbackManager,
                LoginViewModel.FbLoginCallback(viewModel)
            )
        }
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

}