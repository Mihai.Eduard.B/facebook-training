package android.training.myapplication.model

import android.training.myapplication.model.PhotoModel


data class PhotoData (val data: ArrayList<PhotoModel>)

