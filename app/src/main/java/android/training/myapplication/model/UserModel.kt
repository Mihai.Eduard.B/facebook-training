package android.training.myapplication.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserModel (@PrimaryKey val id: String, val name : String)
