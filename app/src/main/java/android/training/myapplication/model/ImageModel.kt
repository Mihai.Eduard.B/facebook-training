package android.training.myapplication.model

import androidx.room.Entity
import androidx.room.PrimaryKey


data class ImageModel(val images: ArrayList<Image>){

    @Entity
    data class Image(var ownerId : String, val height: String?, @PrimaryKey val source: String, val width: String?)
}
