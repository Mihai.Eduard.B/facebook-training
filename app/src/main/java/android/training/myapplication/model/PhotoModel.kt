package android.training.myapplication.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PhotoModel ( @PrimaryKey val id : String, val name : String?)