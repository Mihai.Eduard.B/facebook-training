package android.training.myapplication.utils.background

import android.content.Context
import android.training.myapplication.model.ImageModel
import android.training.myapplication.model.PhotoData
import android.training.myapplication.model.PhotoModel
import android.training.myapplication.model.UserModel
import android.training.myapplication.utils.application.FBApp
import android.util.Log
import androidx.recyclerview.widget.DiffUtil
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class RefreshPhotosWorker(context: Context, workerParams: WorkerParameters) : CoroutineScope,
    Worker(context, workerParams) {

    companion object {
        const val REFRESH_WORKER_USER_ID_KEY = "REFRESH_WORKER_USER_ID_KEY"
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + Job()

    override fun doWork(): Result { //todo: make this refresh photos and update the list in Room -> setup auto refresh with LiveData
        Log.d("TEST", "WORKER DO WORK CALLED")
        val userId = inputData.getString(REFRESH_WORKER_USER_ID_KEY)

        val getPhotoListForUser = GraphRequest(
            AccessToken.getCurrentAccessToken(),
            "/$userId/photos?type=uploaded",
            null,
            HttpMethod.GET,
            GraphRequest.Callback { graphResponse ->
                val photoData = Gson().fromJson(graphResponse.rawResponse, PhotoData::class.java)
                val photoList: ArrayList<PhotoModel> = photoData.data
                photoList.forEach { photoModel ->
                    GraphRequest(
                        AccessToken.getCurrentAccessToken(),
                        "/${photoModel.id}?fields=images",
                        null,
                        HttpMethod.GET,
                        GraphRequest.Callback {
                            val imageList = Gson().fromJson(it.rawResponse, ImageModel::class.java)
                            val image = imageList.images[0]
                            image.ownerId = userId.orEmpty()
                            Log.d(
                                "TEST",
                                "WORKER HAS RETRIEVED Image: $image"
                            )
                            launch {
                                FBApp.roomDatabase.imageDao().insert(image)
                            }
                        }
                    ).executeAsync()
                }
            }
        )
        return Result.success()
    }
}