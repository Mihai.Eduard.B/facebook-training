package android.training.myapplication.utils.application

import android.app.Application
import android.training.myapplication.database.FbRoomDatabase

class FBApp : Application() {
    companion object{
         lateinit var roomDatabase: FbRoomDatabase
    }

    override fun onCreate() {
        super.onCreate()
        roomDatabase = FbRoomDatabase.getDatabase(this.applicationContext)
    }
}