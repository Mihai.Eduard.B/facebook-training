package android.training.myapplication.repository

import android.training.myapplication.model.UserModel
import android.training.myapplication.utils.application.FBApp
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphRequestAsyncTask
import com.facebook.HttpMethod
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class UserRepository : CoroutineScope {
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private val userDao = FBApp.roomDatabase.userDao()

    private val _loggedInUser: MutableLiveData<UserModel> = MutableLiveData()
    val loggedInUser: LiveData<UserModel>
        get() = _loggedInUser


    private lateinit var getLoggedInUserGraphAsyncTask: GraphRequestAsyncTask

    private val getLoggedInUserGraphRequest = GraphRequest(
        AccessToken.getCurrentAccessToken(),
        "/${AccessToken.getCurrentAccessToken().userId}/",
        null,
        HttpMethod.GET,
        GraphRequest.Callback {
            Log.d("TEST", "USER RETRIEVED IS ${it.rawResponse}")
            val user = Gson().fromJson(it.rawResponse, UserModel::class.java)
            launch {
                userDao.insert(user)
            }
        }
    )

    fun saveUser() {
        getLoggedInUserGraphAsyncTask = getLoggedInUserGraphRequest.executeAsync()
    }

    fun cancelOngoingRequests() {
        if (::getLoggedInUserGraphAsyncTask.isInitialized) {
            getLoggedInUserGraphAsyncTask.cancel(true)
        }
    }


}