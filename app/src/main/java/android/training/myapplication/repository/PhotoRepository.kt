package android.training.myapplication.repository

import android.training.myapplication.utils.background.RefreshPhotosWorker
import android.training.myapplication.model.ImageModel
import android.training.myapplication.model.PhotoData
import android.training.myapplication.model.PhotoModel
import android.training.myapplication.model.UserModel
import android.training.myapplication.utils.application.FBApp
import android.training.myapplication.view.fragment.UserPhotosFragment
import android.util.Log
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.work.*
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphRequestAsyncTask
import com.facebook.HttpMethod
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext

class PhotoRepository : CoroutineScope {
    private var job: Job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private lateinit var getUserPhotosGraphAsyncTask: GraphRequestAsyncTask
    val photoSourcesList: MediatorLiveData<ArrayList<String>> = MediatorLiveData()

    private val photoSourcesListFromDb = FBApp.roomDatabase.imageDao().getAllImages()

    init {
        photoSourcesList.addSource(photoSourcesListFromDb) { list ->
            val photoSourcesArrayList: ArrayList<String> = ArrayList()
            list.forEach { photoSourcesArrayList.add(it.source) }
            photoSourcesList.value = photoSourcesArrayList
        }
    }

    private var getPhotosGraphRequest = GraphRequest(
        AccessToken.getCurrentAccessToken(),
        "/${AccessToken.getCurrentAccessToken().userId}/photos?type=uploaded",
        null,
        HttpMethod.GET,
        GraphRequest.Callback { graphResponse ->
            val photoData = Gson().fromJson(graphResponse.rawResponse, PhotoData::class.java)
            val photoList: ArrayList<PhotoModel> = photoData.data
            photoList.forEach { photoModel ->
                GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/${photoModel.id}?fields=images",
                    null,
                    HttpMethod.GET,
                    GraphRequest.Callback {
                        Log.d(
                            "TEST",
                            "ANOTHER REQUEST FOR SPECIFIC PHOTOS FOR THE GLIDE HAS BEEN LAUNCHED"
                        )
                        val imageList = Gson().fromJson(it.rawResponse, ImageModel::class.java)
                        val image = imageList.images[0]
                        image.ownerId = AccessToken.getCurrentAccessToken().userId
                        launch {
                            FBApp.roomDatabase.imageDao().insert(image)
                        }
                    }
                ).executeAsync()
            }
        }
    )

    private fun getPhotoGraphs() {
        getUserPhotosGraphAsyncTask = getPhotosGraphRequest.executeAsync()
    }

    fun cancelOngoingRequests() {
        job.cancel()
        if (::getUserPhotosGraphAsyncTask.isInitialized) {
            getUserPhotosGraphAsyncTask.cancel(true)
        }
    }

    fun saveOrGetSavedPhotoSources(userModel: UserModel) {
        getPhotoGraphs()
    }

    fun getAndRefreshPhotoList() {
        getPhotoGraphs()
    }
}
